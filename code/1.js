// 一、将下面异步代码使用 Promise 的方式改进
// setTimeout(function () {
//   var a = 'hello'
//   setTimeout(function () {
//     var b = 'lagou'
//     setTimeout(function () {
//       var c = 'I Love U'
//       console.log(a + b + c)
//     }, 10)
//   }, 10)
// }, 10)


// 答：
const sleep = (time, value) => {
  return new Promise(resolve => setTimeout(resolve, time, value))
}
sleep(10, 'hello').then(value => {
  return sleep(10, value + 'lagou')
}).then(value => {
  return sleep(10, value + 'I Love U')
}).then(value => {
  console.log(value)
})
