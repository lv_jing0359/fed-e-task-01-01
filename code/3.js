const fp = require('lodash/fp')
const { MayBe, Container } = require('./support')

// 练习 1：使用 fp.add(x,y) 和 fp.map(f,x) 创建一个能让 functor 里的值增加的函数 ex1
let MayBe1 = MayBe.of([5, 6, 1])
let ex1 = () => {
  // 你需要实现的函数。。。
  return fp.map(x => fp.add(x, 1), MayBe1._value)
}
console.log('ex1: ', ex1())



// 练习 2：实现一个函数 ex2，能够使用 fp.first 获取列表的第一个元素
let xs = Container.of(['do', 'ray', 'me', 'fa', 'so', 'la', 'ti', 'do'])
let ex2 = () => {
  // 你需要实现的函数。。。
  return fp.first(xs._value)
}
console.log('ex2: ', ex2())



// 练习 3：实现一个函数 ex3，使用safeProp 和 fp.first 找到 user 的名字的首字母
let safeProp = fp.curry(function (x, o) {
  return MayBe.of(o(x))
})
let user = { id: 2, name: 'Albert' }
let ex3 = () => {
  // 你需要实现的函数。。。
  return safeProp(user.name, fp.first)._value
}
console.log('ex3: ', ex3())


// 练习 4：使用 MayBe 重写 ex4，不要有 if 语句
// let ex4 = function (n) {
//   if (n) {
//     return parseInt(n)
//   }
// }

// 答：
class Ex4MayBe {
  static of (x) {
    return new Ex4MayBe(x)
  }

  isNothing () {
    return this._value === null || this._value === undefined
  }

  constructor (x) {
    this._value = x
  }

  map (fn) {
    return this.isNothing() ? this : MayBe.of(fn(this._value))
  }
}

const ex4 = n => {
  return Ex4MayBe.of(n).map(x => parseInt(x))._value
}
console.log('ex4：', ex4(3.6))